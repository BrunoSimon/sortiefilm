import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:movie_time/movie.dart';

class HttpHelper {
  final String urlKey = 'api_key=8f7fc24a87b69c9a6c94f57602d3a3b8';
  final String urlBase = 'https://api.themoviedb.org/3/movie';
  //example url: https://api.themoviedb.org/3/movie/550?api_key=8f7fc24a87b69c9a6c94f57602d3a3b8
  final String urlUpcoming = '/upcoming?';
  final String urlLanguage = '&language=en-US';

  final String urlSearchBase = 'https://api.themoviedb.org/3/search/movie?';
  final String urlQuery = '&query=';

  /*Future<List> getUpcoming() async {
    /// Return the 20 upcoming movies
    final String upcoming = urlBase + urlUpcoming + urlKey + urlLanguage;
    http.Response result = await http.get(upcoming);

    if (result.statusCode == HttpStatus.ok) {
      final jsonResponse = json.decode(result.body);
      final moviesMap = jsonResponse['results'];
      List movies = moviesMap.map((i) => Movie.fromJson(i)).toList();
      return movies;
    } else {
      return null;
    }
  }*/

  String get upcominUrl{
    return urlBase + urlUpcoming + urlKey + urlLanguage;
  }

  String queryUrl(String title) {
    return urlSearchBase + urlKey + urlQuery + title;
  }

  /*Future<List> findMovies(String title) async {
    final String query = urlSearchBase + urlKey + urlQuery + title;
    http.Response result = await http.get(query);
    if (result.statusCode == HttpStatus.ok) {
      final jsonResponse = json.decode(result.body);
      final moviesMap = jsonResponse['results'];
      List movies = moviesMap.map((i) => Movie.fromJson(i)).toList();
      return movies;
    } else {
      return null;
    }
  }*/

  Future<List> listMovies(String url) async {
    http.Response result = await http.get(url);
    if (result.statusCode == HttpStatus.ok) {
      final jsonResponse = json.decode(result.body);
      final moviesMap = jsonResponse['results'];
      List movies = moviesMap.map((i) => Movie.fromJson(i)).toList();
      return movies;
    } else {
      return null;
    }
  }

}
