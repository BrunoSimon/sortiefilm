import 'package:flutter/material.dart';
import 'package:movie_time/movie_list.dart';

void main() => runApp(MyMovies());

String appTitle = 'Movie Time';

class MyMovies extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  MovieList();
  }
}
